Project created by Patrick Nicholson and Henry Chen

Run the program by running src/cs4621/GPUray/GPURayApp.java. It will need access to all external libraries included in this project.

Look up and down using 'U' and 'J', left and right using 'H' and 'K', and move around using the arrow keys.