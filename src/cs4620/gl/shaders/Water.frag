#version 120

// You May Use The Following Functions As RenderMaterial Input
// vec4 getDiffuseColor(vec2 uv) // samples fire.png
// vec4 getNormalColor(vec2 uv)  // samples noise.png

uniform float time;

const vec3 texture_scales = vec3(1.0, 2.0, 3.0);
const vec3 scroll_speeds = vec3(.1, .1, .1);

varying vec2 fUV;
varying vec3 fPos;

void main() {
  // TODO#PPA2 SOLUTION START
  vec2 uv1 = texture_scales.x * fUV;
  vec2 uv2 = texture_scales.y * fUV;
  vec2 uv3 = texture_scales.z * fUV; 
  
  uv1.y += scroll_speeds.x * time;
  uv1.x = mod(uv1.x , 1);
  uv1.y = mod(uv1.y , 1);
  uv2.y += scroll_speeds.y * time;
  uv2.x = mod(uv2.x , 1);
  uv2.y = mod(uv2.y , 1);
  uv3.y += scroll_speeds.z * time;
  uv3.x = mod(uv3.x , 1);
  uv3.y = mod(uv3.y , 1);
  
  vec4 n1 = getNormalColor(uv1);
  vec4 n2 = getNormalColor(uv2);
  vec4 n3 = getNormalColor(uv3);
  vec2 t = fPos.xy;

  
  vec4 sample = (n1 + n2 + n3) / 3;
  vec4 color = (1-pow(t.y,4)) * getDiffuseColor(sample.xy) + pow(t.y,4) * vec4(getDiffuseColor(sample.xy).zyxw );
  color = color * (1-pow(t.y,10));
  gl_FragColor = color;
  // SOLUTION END
}