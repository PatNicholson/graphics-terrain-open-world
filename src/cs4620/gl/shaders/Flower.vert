#version 120

// Lighting Information
const int MAX_LIGHTS = 16;
uniform vec3 lightPosition[MAX_LIGHTS];

// RenderCamera Input
uniform mat4 mViewProjection;

// RenderObject Input
uniform mat4 mWorld;
uniform mat3 mWorldIT;

// RenderMesh Input
attribute vec4 vPosition; // Sem (POSITION 0)
attribute vec3 vNormal; // Sem (NORMAL 0)

varying vec3 fN; // Normal at the vertex in world-space coordinates
varying vec4 worldPos; // Vertex position in world-space coordinates

const float PI = 3.1415926535;
// The height of the flower in object coordinates
const float height = 3.0;

void main() {
  float L_x = sqrt(lightPosition[0].x * lightPosition[0].x +
                   lightPosition[0].z * lightPosition[0].z);
  float L_y = lightPosition[0].y;

  if (L_x < 0.00001) {
    // If the light is too close to directly above the flower, the math
    // for the bending of the flower becomes unstable, so just render
    // the unbent flower

    // TODO#PPA2 SOLUTION START

    // Calculate Point In World Space
    // Calculate Projected Point
    // Calculate normal
    worldPos = mWorld * vPosition;
	fN = normalize((mWorldIT * vNormal).xyz);
	
      gl_Position = mViewProjection * worldPos;
    // SOLUTION END

  } else {
    // These matrices map between the frame of the flower mesh's vertices and a frame in which
    // the light lies on the z>0 part of the x-y plane
    mat4 frameToObj = mat4(lightPosition[0].x / L_x, 0, lightPosition[0].z / L_x, 0,
                           0,                        1,  0,                        0,
                           -lightPosition[0].z / L_x, 0,  lightPosition[0].x / L_x, 0,
                           0,                        0,  0,                        1);

    // Find inverse of frameToObj
    mat4 objToFrame = transpose(frameToObj);

    // TODO#PPA2 SOLUTION START

    // Calculate the angle theta from the diagram in pa2a.pdf
    // Calculate the value "R" and its inverse according to the formula
    // for the bending of the flower
    // find vertex/normal in axis-aligned frame
    // find transformed vertex/normal in local frame
    // map transformed vertex/normal back to object frame and to eye/screen space
      vec4 lightPos = objToFrame * vec4(lightPosition[0],1);
      float theta = max(atan(lightPos.y, lightPos.x),0);
      float phi = 3.1415926535 / 2 - theta;
      float r = 3/phi;
      vec4 pos = objToFrame * vPosition;
      float r2 = r - pos.x;
      float phi2 = pos.y/height*phi;
      vec4 pos2 = vec4(r - r2 * cos(phi2),(r2) * sin(phi2),pos.z,pos.w);
      vec4 normal2 = objToFrame * vec4(vNormal,1);
      mat2 rot =mat2(cos(-phi2),sin(-phi2),
      			 sin(phi2),cos(-phi2));
      normal2 = vec4(rot * normal2.xy , normal2.z,1);
      pos2 = frameToObj * pos2;
      normal2 = frameToObj* normal2;
     
      worldPos = mWorld * pos2;
	  fN = normalize((mWorldIT * normal2.xyz).xyz);
	
      gl_Position = mViewProjection * worldPos;
    

    // SOLUTION END
  }
}
