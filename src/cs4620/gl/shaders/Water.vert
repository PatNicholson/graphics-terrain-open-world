#version 120

// RenderCamera Input
uniform mat4 mViewProjection;

// RenderObject Input
uniform mat4 mWorld;

// RenderMesh Input
attribute vec4 vPosition; // Sem (POSITION 0)
attribute vec3 vNormal; // Sem (NORMAL 0)
attribute vec2 vUV; // Sem (TEXCOORD 0)

const vec3 texture_scales = vec3(1.0, 1.0, 1.0);
const vec3 scroll_speeds = vec3(.1, .1, .1);
uniform float time;

varying vec2 fUV;
varying vec3 fPos;

void main() {
	fUV = vUV;
	vec2 uv1 = texture_scales.x * fUV;
  	vec2 uv2 = texture_scales.y * fUV;
    vec2 uv3 = texture_scales.z * fUV; 
  
  	uv1.x += scroll_speeds.x * time;
  	uv1.x = mod(uv1.x , 1);
  	uv1.y = mod(uv1.y , 1);
  	uv2.x -= scroll_speeds.y * time;
  	uv2.x = mod(uv2.x , 1);
  	uv2.y = mod(uv2.y , 1);
  	uv3.x += scroll_speeds.z * time;
  	uv3.x = mod(uv3.x , 1);
  	uv3.y = mod(uv3.y , 1);
  	vec2 sample = (uv1 + uv2 + uv3) / 3;
	vec4 nm = getNormalColor(mod(sample * 3.1415, 1));
	//float disp = (nm.x + nm.y + nm.z)/3;
	float disp = ((nm.x + nm.y + nm.z)/3)*cos(time+sample.x+sample.y);
	vec4 worldPos = mWorld * (vPosition + .05*disp*vec4(normalize(vec3(vNormal.x,vNormal.y,vNormal.z)),0));
	//vec4 worldPos = mWorld * vPosition;
	
    fPos = vPosition.xyz;
    //worldPos = (mWorld * vPosition);
	gl_Position = mViewProjection * worldPos;
}