/**
 * @author Jimmy, Andrew 
 */

package cs4620.splines;
import java.util.ArrayList;

import egl.math.Matrix4;
import egl.math.Vector4;
import egl.math.Vector2;

public class CatmullRom extends SplineCurve {

	public CatmullRom(ArrayList<Vector2> controlPoints, boolean isClosed,
			float epsilon) throws IllegalArgumentException {
		super(controlPoints, isClosed, epsilon);
	}

	@Override
	public CubicBezier toBezier(Vector2 p0, Vector2 p1, Vector2 p2, Vector2 p3,
			float eps) {
		//TODO A5
		//SOLUTION
		float c = 1.0f/6.0f;
		Matrix4 mat = new Matrix4(
				0,1,0,0,
				-c,1,c,0,
				0,c,1,-c,
				0,0,1,0					
				);
		Vector4 x = new Vector4(p0.x,p1.x,p2.x,p3.x);
		Vector4 y = new Vector4(p0.y,p1.y,p2.y,p3.y);
		mat.mul(x);
		mat.mul(y);
		
		return new CubicBezier(new Vector2(x.x,y.x), new Vector2(x.y,y.y), new Vector2(x.z,y.z), new Vector2(x.w,y.w), eps);
		//END SOLUTION0
	}
}
