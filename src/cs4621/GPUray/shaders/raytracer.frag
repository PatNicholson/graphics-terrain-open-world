	#version 330

// Input from vertex shader (location on our camera plane)
in vec2 vUV;

// Constants for defining uniform array sizes
// In GLSL 330, uniforms arrays must have constant length
const int MAX_VERTS = 128;
const int MAX_TRIS = 256+64+8;
const int MAX_COLORS = 16;
const float maxRender = 120.0f;
const mat2 rotate2D = mat2(1.3623, 1.7531, -1.7131, 1.4623);
// Axis-Aligned Bounding Box
const vec3 aabb_max = vec3(5000, 5000, 5000);
const vec3 aabb_min = vec3(-5000, -5000, -5000);

// The output color, of form (red,green,blue,alpha)
// The job of your fragment shader is to set this correctly.
out vec4 vFragColor;

// Uniforms
// TODO#PPA1 Solution Start
// Add any uniforms you need to pass in from java
uniform vec4 backgroundColor;
uniform vec3 lightPos;
//Intensity values are between 0 and 1
uniform vec3 lightIntensity;
uniform mat3 invmVP;
uniform vec3 camx;
uniform vec3 camy;
uniform vec3 camz;
uniform ivec4[MAX_TRIS] ibTris;
uniform vec3[MAX_VERTS] fbVerts;
uniform vec3[MAX_COLORS] fbColors;
uniform float viewWidth;
uniform float viewHeight;
uniform float projDistance;
uniform vec3 viewPoint;
uniform int debugState;
uniform float time;
// Solution End

// Function to return the intersection of a ray with a box.
// Returns a vec2 in which the x value contains the t value at the near intersection
// and the y value contains the t value at the far intersection.

vec2 intersectCube(vec3 origin, vec3 ray, vec3 cube_min, vec3 cube_max) {
  // TODO#PPA1 Solution Start

  // Implement axis-aligned box intersection here
    float txmax = (cube_max.x - origin.x) /ray.x;
	float txmin = (cube_min.x - origin.x) /ray.x;
	float tymax = (cube_max.y - origin.y) /ray.y;
	float tymin = (cube_min.y - origin.y) /ray.y;
	float tzmax = (cube_max.z - origin.z) /ray.z;
	float tzmin = (cube_min.z - origin.z) /ray.z;
	txmax = max(txmax, 0);
	tymax = max(tymax, 0);
	tzmax = max(tzmax, 0);
	txmin = max(txmin, 0);
	tymin = max(tymin, 0);
	tzmin = max(tzmin, 0);
		
		
	//System.out.println("T:" + txmax + ":" + txmin);
		
	float txenter = min(txmin,txmax);
	float txexit = max(txmin, txmax);
	float tyenter = min(tymin,tymax);
	float tyexit = max(tymin, tymax);
	float tzenter = min(tzmin,tzmax);
	float tzexit = max(tzmin, tzmax);
		
	float tenter = max(txenter,tyenter);
	tenter = max(tenter,tzenter);
	float texit = min(txexit,tyexit);
	texit = min(texit,tzexit);
		
	vec2 inter =  vec2(tenter,texit);
	//System.out.println(tenter + ":" + texit);
	return inter;
	
	
  // Solution End
}

// Gets the direction given a 2D position p,
// and a Camera with basis U, V, W and projection distance d.
vec3 get_direction(vec2 p, vec3 U, vec3 V, vec3 W, float d) {
  // TODO#PPA1 Solution Start

  // Return the direction towards a point p on UV plane a distance
  // d away from the camera along W.
    	
   float inU2 = p.x*(viewWidth/2);
   float inV2 = p.y*(viewHeight/2);
   return normalize(((-1)*d*W)+(U*inU2)+(V*inV2));
    	
  // Solution End
}



float Hash12(vec2 p)
{	
	p  = fract(p / vec2(3.07965, 7.4235));
    p += dot(p.xy, p.yx+19.19);
    return fract(p.x * p.y);
}

float Noise( in vec2 x )
{
    vec2 p = floor(x);
    vec2 f = fract(x);
    f = f*f*(3.0-2.0*f); //simulates curved interpolation
    
    float res = mix(mix( Hash12(p), Hash12(p + vec2(1,0)),f.x),
                    mix( Hash12(p + vec2(0,1)), Hash12(p + vec2(1,1)),f.x),f.y);
    return res;
}


float fresnel(vec2 p ,int samples, int depth, float w){
	p = samples * p;
	float res = 0.0f;
	for(int i = 0; i < depth; i++){
		res += w * Noise(p);
		w = -w * .4;
		p = rotate2D * p;
	}
	return res;

}




float height(float x, float z){
	
	vec2 p = vec2(x,z);	
	vec2 pos = p*0.05;
	float w = (Noise(pos*.25)*0.75+.15);
	w = 66.0 * w * w;
	vec2 dxy = vec2(0.0, 0.0);
	
	float f = .0;
	//for (int i = 0; i < 5; i++)
	//{
	//	f += w * Noise(pos);
	//	w = -w * 0.4;	//...Flip negative and positive for variation
	//	pos = rotate2D * pos;
	//}
	
	float ff = Noise(pos*.002);
	f += pow(abs(ff), 5.0)*1000.;
	
	f += fresnel(pos,1,5,w); 
	
	return f ;
	//return -1;//sin(x)*sin(z) * .5;
}


float height2(float x, float z){
	vec2 p = vec2(x,z);	
	vec2 pos = p*0.05;
	float w = (Noise(pos*.25)*0.75+.15);
	w = 66.0 * w * w;
	vec2 dxy = vec2(0.0, 0.0);
	float f = .0;
	
	float ff = Noise(pos*.002);
	f += pow(abs(ff), 5.0)*1000;
	
	f += fresnel(pos,1,10,w);
	return f ;
	




}


float sea_octave(vec2 uv, float choppy) {
    uv += Noise(uv); 
    vec2 wv = 1.0-abs(sin(uv));
    vec2 swv = abs(cos(uv));    
    wv = mix(wv,swv,wv);
    return pow(1.0-pow(wv.x * wv.y,0.65),choppy);
}

float wheight(float x, float z){
	//int t = int(int(time)/100);
	//return .05*sin(10*x+t)*cos(10*z+t)-.2;
	float freq = .8;
    float amp = .3;
    float choppy = 7.0;
    mat2 octave_m = mat2(1.6,1.2,-1.2,1.6);
    vec2 uv = vec2(x,z); uv.x *= .75;
    
    float d, h = 0.0;
    for(int i = 0; i < 5; i++){           
	    d = sea_octave((uv+time/1000)*freq,choppy);
	    d += sea_octave((uv-time/1000)*freq,choppy);
	    h += d * amp;        
	    uv *= octave_m; freq *= 1.9; amp *= 0.22;
	    choppy = mix(choppy,1.0,0.2);
    }
    return h;
}

vec3 getSkyColor(vec3 e) {
	
    vec3 dirToSun = normalize(lightPos);
	float angle = dot(dirToSun,e)/4+.75;
	float nearTop = dot(dirToSun,vec3(0,1,0));
	float nearHorizon = abs(dot(dirToSun,vec3(0,0,1)));
    vec3 ret;
    ret = mix(vec3(0,0,0),mix(vec3(.53,.81,.98),vec3(0,.75,1.0),(1-.9*mix(angle,nearHorizon,.5))),.6+.4*nearTop);
    if(angle > .993){
      ret = mix(ret,vec3(1,1,.9*(.7+.3*pow((1-nearHorizon),20))),pow(((angle-.993)/.007),25));
    }
    return ret;
    //return vec3(.0,.8,1);

}

float intersectTerrain(vec3 origin, vec3 dir){
	float delt = .1f;
	const float mint = 0.01f;	
	const float maxt = maxRender;
	float lh = 0.0f;
	float ly = 0.0f;
	for(float t = mint; t < maxt; t += delt){
	    vec3 p = origin + dir * t;
	    float h = height(p.x,p.z);
		 if(p.y < h){	
			float resT = t - delt + delt * (lh-ly) / (p.y - ly - h + lh);
			return resT;
		}
		lh = h;
		ly = p.y;
		delt = max(0.01, 0.3*(p.y-h)) + (t*0.0065);
	}
	return -1;		
}

float intersectWater(vec3 origin, vec3 dir){
	float delt = .1f;
	const float mint = 0.01f;	
	const float maxt = maxRender;
	float lh = 0.0f;
	float ly = 0.0f;
	for(float t = mint; t < maxt; t += delt){
	    vec3 p = origin + dir * t;
	    float h = wheight(p.x,p.z);
		 if(p.y < h){	
			float resT = t - delt + delt * (lh-ly) / (p.y - ly - h + lh);
			return resT;
		}
		lh = h;
		ly = p.y;
		delt = max(0.01, 0.3*(p.y-h)) + (t*0.0065);
	}
	return -1;		
}




vec3 applyFog(vec3 col, float t,vec3 dir){
	return mix (col , getSkyColor(dir) , pow(t/maxRender,2)); 
}





//vec4 intersectSphere(vec3 origin, vec3 dir){

vec3 getNormal(vec3 p){
	float eps = 0.001f;
	vec3 n = vec3( height2(p.x-eps,p.z) - height2(p.x+eps,p.z),
                         2.0f*eps,
                         height2(p.x,p.z-eps) - height2(p.x,p.z+eps) );
	return normalize(n);
}	


float compute_shadow(vec3 origin, vec3 dir ) {
	float delt = .1f;
	float k = 2;
	const float mint = 0.01f;	
	const float maxt = maxRender;
	float lh = 0.0f;
	float ly = 0.0f;
	float res = 1.0f;
	for(float t = mint; t < maxt; t += delt){
	    vec3 p = origin + dir * t;
	    float h = height(p.x,p.z);
		 if(p.y < h){	
			float resT = t - delt + delt * (lh-ly) / (p.y - ly - h + lh);
			res = min(res, k*(p.y - h)/resT);
			break; 
		}
		//delt = .01f*t;
		res = min(res, k*(p.y - h)/t);
		lh = h;
		ly = p.y;
		delt = max(0.01, 0.3*(p.y-h)) + (t*0.0065);
	}
	
  	return res;

  // Solution End
}


// Function to compute lambertian shading
vec3 shade_lambertian(vec3 normal, vec3 light_dir, vec3 mesh_color) {
							
  return max(0.0, dot(normalize(normal),normalize(light_dir))) * mesh_color;

}

vec3 terrainColor(vec3 origin, vec3 dir, float t){
	vec3 p = origin + dir * t;
	vec3 n = getNormal(p);
	vec3 mat;
	if(n.y > .75){ //grass
		//mat = mix(vec3(.3,.5,0), vec3(.5,.6,.15), fresnel(p.xz * 20 ,2,.7));
		mat = vec3(fresnel(p.xz , 100 ,2,.1) +.2, fresnel(p.xz ,  5 ,2,.2) +.4, fresnel(p.xz , 5 ,2,.05) +.1); 
		if(p.y < 1){
			mat = mix(mat, vec3(fresnel(p.xz , 100 ,2,.15) +.6,fresnel(p.xz , 5 ,2,.15) +.6,fresnel(p.xz , 100 ,2,.1) +.3), 1-p.y);
		}
	}
	else { //mountain 
		mat = vec3(fresnel(p.xz , 100 ,2,.1) +.4,fresnel(p.xz , 20 ,2,.05) +.15,fresnel(p.xz , 5 ,2,.05) +.15);
	}
	vec3 col = shade_lambertian(n, lightPos - p , mat);
	vec3 fogged = applyFog(col,t,dir);
	return fogged;
}

vec3 getWaterColor(vec3 e, vec3 origin, float depth) {

	float refindex = 1.34;
	float thetax = 3.14159/2-atan(wheight(e.x+.1,e.z)-wheight(e.x,e.z));
	vec3 outgoing = normalize(origin - e);
	vec3 normal = normalize(vec3(-cos(thetax),sin(thetax),0));
	float ro = pow((refindex-1.0)/(refindex+1.0),2);
	float costheta1 = dot(outgoing,normal);
	float fresnel = ro + (1-ro)*(pow(1-costheta1,5));
	vec3 s2 = outgoing - normal*costheta1*refindex*(-1);
	s2 = normalize(s2 - normal*sqrt(1-pow(length(s2),2)));
	float refract =  intersectTerrain(e,s2);
	vec3 s3 = normalize(2*dot(outgoing,normal)*normal-outgoing);
	float reflect =  intersectTerrain(e,s3);
	vec3 refractColor = mix (vec3(0,.545,.545), vec3(.7,.7,.4),1- min(1.0,pow((-depth ),.5))) ;
	vec3 reflectColor = getSkyColor(2*dot(outgoing,normal)*normal-outgoing);
	if(reflect > 0){ 
		reflectColor = terrainColor(e,s3,reflect) * .5;
	}
	return 3*fresnel*reflectColor + (1-3*fresnel)*refractColor;
	//return vec3(0,0,e.y);
	//vec3(.05 * t,0.05*t,.05*t);
}

// Generates the eye ray for the given camera and a 2D position.
void setup_camera(vec2 uv, inout vec3 eyeRayOrigin, inout vec3 eyeRayDir, 
                  inout vec3 camU, inout vec3 camV,  inout vec3 camW,                  
                  inout float camd) {

  // TODO#PPA1 Solution Start

  // 1) Set the eyeRayOrigin to the view point of your camera
  // 2) Set camU to the first column of invMVP
  //    Set camV to the second,
  //    Set camW to the third.
  //    Set camd to the projection distance
  // 3) Set eyeRayDir the direction of the eyeRay 
  camU = invmVP[0];
  camV = invmVP[1];
  camW = invmVP[2];
  camd = projDistance;
  float h = max(height(viewPoint.x,viewPoint.z),0);
  eyeRayOrigin = vec3(viewPoint.x,3 + h,viewPoint.z);
  eyeRayDir = get_direction(uv,camU,camV,camW,camd);
  					
  					 

  // Solution End
}

void main() {

  //set the maximum t value
  float t = 100000000;

  // TODO#PPA1 Solution Start

  // Set the initial fragment colour to the scene's background colour
  // You will have to make a new uniform for this
  //vFragColor = vec4(1,0,0,1);
  //vFragColor = backgroundColor;

  // Solution End

  
  // Setup the camera
  vec3 origin;
  vec3 dir;
  vec3 camU;
  vec3 camV;
  vec3 camW;
  float camd;
  setup_camera(vUV, origin, dir, camU, camV, camW, camd);
  

  // check if the ray intersects the scene bounding box
  vec2 tNearFar = intersectCube(origin, dir, aabb_min, aabb_max);
  

  if (tNearFar.x<tNearFar.y) {
    t = tNearFar.y+1; //offset the near intersection to remove the depth artifacts

    // TODO#PPA1 Solution Start

    // 1) Trace ray through the whole triangle list and see if we have a intersection
    // 2) If there is a valid intersection:
    //    a) Get intersection point
    //    b) Get direction from the intersection point to the light
    //    c) Check for shadows
    //    d) Set vFragColor according to the current debug state
    //       Debug state 0: color = lambertian + shadows
    //                      remember to include the mesh color, the diffuse modifier,
    //                      and the shadowing in your final color
    //       Debug state 1: color = normal directions
    //       Debug state 2: color = intersection location
    //       Debug state 3: color = white if not shadowed, else 50% grey
	
	vec4 intersect = vec4(-1,0,0,0);
	vec4 fintersect = vec4(-1,0,0,0);
	vec3 normal = vec3(0,0,0);
	vec3 fnormal = vec3(0,0,0);
	vec3 temp = vec3(0,0,0);
	
	float t = -1;
	t = intersectTerrain(origin,dir);
	vec3 p = origin + dir * t;
	
	fnormal = vec3(0,1,0);
	
	if(t > -0.001){
		//temp = shade_lambertian(fnormal, lightPos - (origin+(dir*t)) , vec3(1,0,.5));
		//vFragColor = vec4(.05 * t,0.05*t,.05*t,1); //vec4(temp.x,temp.y,temp.z,1) * compute_shadow(origin+(dir*t), lightPos-(origin+(dir*t)));	
		if(p.y > 0){
			vFragColor = vec4(terrainColor(origin,dir,t),1);// * compute_shadow(origin+(dir*t), lightPos-(origin+(dir*t)));
		}
		else { 
			float tw = intersectWater(origin,dir);
			vFragColor = vec4(applyFog(getWaterColor(origin + tw * dir,origin,p.y),tw,dir),1);
			if(tw < 0){
				vFragColor = vec4(0,origin.y + tw * dir.y,0,1);
			}
	
		}
	
	}
	else {
		vFragColor = vec4(getSkyColor(dir),1);
	}
    // Solution End
  }
 
}
	